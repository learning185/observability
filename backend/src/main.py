import uvicorn
from fastapi import FastAPI
from prometheus_client import make_asgi_app
from src.endpoints import (
    counter_router,
    counter_with_labels_router,
    gauge_router,
    hello_world_router,
)

routers = [hello_world_router, counter_with_labels_router, counter_router, gauge_router]
app = FastAPI(title="backend to observe")
for router in routers:
    app.include_router(router)

metrics_app = make_asgi_app()
app.mount("/metrics", metrics_app)

if __name__ == "__main__":
    uvicorn.run(
        "src.main:app",
        host="0.0.0.0",
        port=8500,
        use_colors=True,
        reload=True,
    )
