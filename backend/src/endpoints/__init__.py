from .counters import counter_router as counter_router
from .counters_with_labels import (
    counter_with_labels_router as counter_with_labels_router,
)
from .gauges import gauge_router as gauge_router
from .hello_world import hello_world_router as hello_world_router
