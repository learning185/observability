from fastapi import APIRouter
from prometheus_client import Gauge

gauge_router = APIRouter()


g = Gauge(
    "try_out_gauge",
    "try out functionality off gauge",
)


@gauge_router.get("/gauge/inc/")
async def inc_gauge():
    g.inc()
    return {"message": f"counter at {g._value.get()}"}


@gauge_router.get("/gauge/inc/{gauge_inc}")
async def inc_gauge_with(gauge_inc: int):
    g.inc(gauge_inc)
    return {"message": f"counter at {g._value.get()}"}


@gauge_router.get("/gauge/dec/")
async def dec_gauge():
    g.dec()
    return {"message": f"counter at {g._value.get()}"}


@gauge_router.get("/gauge/dec/{gauge_dec}")
async def dec_gauge_with(gauge_dec: int):
    g.dec(gauge_dec)
    return {"message": f"counter at {g._value.get()}"}


@gauge_router.get("/gauge/set/")
async def set_gauge(set_gauge: int):
    g.set(set_gauge)
    return {"message": f"counter at {g._value.get()}"}
