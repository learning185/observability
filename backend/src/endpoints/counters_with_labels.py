from fastapi import APIRouter
from prometheus_client import Counter

counter_with_labels_router = APIRouter()


c = Counter(
    "try_out_counter_labels",
    "try out functionality off counter_with_labels",
    ["method", "endpoint"],
)

c.labels("get", "/count/with_label/")
c.labels("post", "/count/with_label/create/")


@counter_with_labels_router.get("/count/with_label/")
async def get_count_with_label():
    c.labels("get", "/count/with_label/").inc()
    return {
        "message": f"counter at {c.labels('get','/count/with_label/')._value.get()}"
    }


@counter_with_labels_router.post("/count/with_label/create/")
async def post_count_with_label():
    c.labels("post", "/count/with_label/create/").inc()
    return {
        "message": f"counter at {c.labels('post','/count/with_label/create/')._value.get()}"
    }


@counter_with_labels_router.post("/count/with_label/create/{custom_label}")
async def post_count_with_label_custom(custom_label: str):
    c.labels("post", f"/count/with_label/create/{custom_label}").inc()
    return {
        "message": f"counter at {c.labels('post',f'/count/with_label/create/{custom_label}')._value.get()}"
    }
