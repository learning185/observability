from fastapi import APIRouter
from prometheus_client import Counter

counter_router = APIRouter()


@counter_router.get("/hello/world")
async def hello_world():
    return {"message": "hello world"}


c = Counter("try_out_counter", "try out functionality off counter")


@counter_router.get("/count/")
async def count():
    c.inc()
    return {"message": f"counter at {c._value.get()}"}


@counter_router.get("/count/{count_inc}/")
async def count_with_inc_mount(count_inc: int):
    c.inc(count_inc)
    return {"message": f"counter inc with {count_inc} - counter at {c._value.get()}"}
