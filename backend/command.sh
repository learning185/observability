#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

echo "cmd: start the application"
python -m src.main
